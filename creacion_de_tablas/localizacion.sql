/************************************************************************************************
 * 										LOCALIZACIONES											*
 ************************************************************************************************/

-- CREACION TABLA: PAISES
 create table if not exists paises(
	codigo int not null primary key, 
	codigo_alfa varchar(3) unique not null,
	nombre varchar(50) unique not null
);


-- CREACION TABLA: PROVINICIAS 
create table if not exists provincias(
	codigo int not null primary key,
	nombre varchar(50) not null,
	codigo_pais int not null references paises(codigo) 
);


-- CREACION TABLA: CIUDADES 
create table if not exists ciudades(
	codigo int not null primary key,
	nombre varchar(100) not null,
	codigo_provincia int not null references provincias(codigo)
);


-- CREACION TABLA: DIRECCIONES 
create table if not exists direcciones(
	id serial primary key,
    calle varchar(50) not null,
	numero int not null,
	piso int,
	depto varchar(4),
	ciudad int references ciudades(codigo), 
    unique (calle, numero, piso, depto, ciudad)
);
