/************************************************************************************************
 * 										INCIDENCIAS												*
 ************************************************************************************************/

create table if not exists incidencias(
	id serial primary key,
	fecha timestamp default 'now',
	descripcion text not null 
);