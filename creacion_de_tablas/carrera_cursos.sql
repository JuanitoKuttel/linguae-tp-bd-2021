/************************************************************************************************
 * 								CARRERAS, CURSOS y DERIVADOS									*
 ************************************************************************************************/

drop domain if exists d_idiomas;
create domain d_idiomas as varchar(25)
check (value in ('INGLES','ALEMAN','ITALIANO'));

drop domain if exists d_niveles;
create domain d_niveles as varchar(25)
check (value in ('BASICO','MEDIO','AVANZADO','ESPECIALISTA'));

drop domain if exists d_modalidades;
create domain d_modalidades as varchar(25)
check (value in ('PRESENCIAL','DISTANCIA','HIBRIDO'));


-- CREACION TABLA: CARRERAS 
create table if not exists carreras(
	id serial primary key,
	nombre varchar(50) not null
);


-- CREACION TABLA: CURSOS 
create table if not exists cursos(
	id serial primary key,
	nombre varchar(50) not null,
	idioma d_idiomas not null,
	nivel d_niveles not null,
	sede int references sedes(id) 
);


-- CREACION DE LA TABLA MODALIDAD_CURSO
create table if not exists modalidades(
	id serial primary key,
	duracion_meses int not null,
	costo money not null,
	modalidad d_modalidades not null
);


--CREACION DE LA TABLA DICTADOS
create table if not exists dictados(
	id serial primary key,
	modalidad int references modalidades(id) not null,
	curso int references cursos(id) not null,
	fec_inicio date not null,
	fec_fin date not null
);


 -- CREACION DE LA TABLA ANO LECTIVO
create table if not exists ano_lectivo(
	id serial primary key,
	carrera int references carreras(id) not null,
	ano int not null,
	monto_inscripcion money not null
);

-- CREACION DE LA TABLA CURSOS CARRERAS
create table if not exists cursos_carreras(
	id_carrera int not null references carreras(id),
	id_curso int not null references cursos(id)
);


-- CREACION DE LA TABLA CORRELATIVIDADES
create table if not exists correlatividades(
	id_curso int not null references cursos(id),
	id_correlativo int not null references cursos(id)
); 