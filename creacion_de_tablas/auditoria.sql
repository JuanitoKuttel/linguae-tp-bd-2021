/************************************************************************************************
 * 									        AUDITORIA       									*
 ************************************************************************************************/
-- CREACION TABLA: REGISTROS DOCENTES
/* 
    Esta tabla nos permitira registrar el historial 
    de los diferentes docentes de las sedes que se
    inscriben a un dictado de curso como docentes
    o evaluadors con la ayuda de un trigger en las
    tablas correspondientes.
*/ 
create table if not exists registro_docentes(
    registro serial primary key,
    docentes int not null references docentes(legajo),
    descripcion varchar(4000) not null
);

-- CREACION TABLA: REGISTROS CARGOS
/* 
    Esta tabla nos permitira registrar el historial 
    de los diferentes cargos de las sedes mediante
    un trigger en la insercion y update de la tabla
    cargos.
*/ 
create table if not exists  (
    registro serial primary key,
    persona int not null references personas(dni),
    descripcioon varchar(4000) not null
);
