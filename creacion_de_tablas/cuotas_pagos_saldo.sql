/************************************************************************************************
 * 									CUOTAS / PAGOS / SALDO										*
 ************************************************************************************************/

-- CREACION DE LA TABLA CUOTAS CURSOS
create table if not exists cuotas_cursos_alumno(
	id serial primary key,
	costo money not null,
	curso_dictado int not null references dictados(id),
	vencimiento date not null,
	alumno int not null references alumnos(legajo)
);


-- CREACION DE LA TABLA CUOTAS INSCRIPCIONES CARRERAS
create table if not exists cuotas_insc_carrera_alumno(
	id serial primary key,
	costo money not null,
	ano int not null references ano_lectivo(id),
	vencimiento date not null,
	alumno int not null references alumnos(legajo)
);


-- CREACION DE LA TABLA PAGOS CURSOS
create table if not exists pagos_cursos(
	couta_curso int not null references cuotas_cursos_alumno(id),
	monto money not null,
	fecha date default 'now'
);


-- CREACION DE LA TABLA PAGOS CARRERAS
create table if not exists pagos_carrera(
	cuota_insc int not null references cuotas_insc_carrera_alumno(id),
	monto money not null,
	fecha date default 'now' 
);

-- CREACION DE LA TABLA SALDOS
create table if not exists saldos_alumnos(
	alumno int not null references alumnos(legajo),
	saldo money
);