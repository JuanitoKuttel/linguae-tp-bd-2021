/************************************************************************************************
 * 									PERSONAS Y DERIVADOS										*
 ************************************************************************************************/

drop domain if exists d_rol_docente;
create domain d_rol_docente as varchar(30)
check (value in ('CLASES', 'EVALUADOR'));

drop domain if exists d_cargos;
create domain d_cargos as varchar(30)
check (value in ('SECRETARIO', 'DIRECTOR', 'DIRECTOR GENERAL'));

-- CREACION TABLA: PERSONAS
create table if not exists personas(
	dni int not null primary key,
	nombre varchar(40) not null,
	apellido varchar(40) not null,
	fec_nacimiento date not null
);


-- CREACION TABLA: ALUMNOS
create table if not exists alumnos(
	legajo serial primary key,
	dni int unique not null references personas(dni),
);


-- CREACION TABLA: ALUMNOS <-> SEDE
create table if not exists alumnos_sedes(
    alumno int not null references alumnos(legajo),
    sede int not null references sedes(id)
);


-- CREACION TABLA: DOCENTES
create table if not exists docentes(
	legajo serial primary key,
	dni int unique not null references personas(dni)
);


-- CREACION TABLA: DOCENTES <-> SEDES
create table if not exists docentes_sedes(
    docente int not null references docentes(legajo),
    sede int not null references sedes(id)
);


-- CREACION TABLA: DOCENTES <-> CURSOS
create table if not exists docentes_cursos(
	docente int not null references docentes(legajo),
	curso int not null references dictados(id)
	rol d_rol_docente not null
);


-- CREACION TABLA: CARGOS
create table if not exists cargos(
	dni int primary key references personas(dni),
	cargo d_cargos not null,
    idioma d_idiomas,
	sede int references sedes(id),
    unique (dni, sede)
);