/************************************************************************************************
 * 										INSCRIPCIONES											*
 ************************************************************************************************/

-- CREACION DE LA TABLA INSCRIPCIONES
create table if not exists inscripciones_carrera(
	carrera int references carreras(id),
	alumno int references alumnos(legajo)
);

create table if not exists inscripciones_curso(
	curso_dictado int references dictados(id),
	alumno int references alumnos(legajo)
);