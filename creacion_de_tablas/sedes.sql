/************************************************************************************************
 * 											SEDES												*
 ************************************************************************************************/

-- CREACION TABLA: SEDES 
create table if not exists sedes(
	id serial primary key,
	nombre varchar(100) unique,
	direccion int references direcciones(id)
);