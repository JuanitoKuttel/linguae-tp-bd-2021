/************************************************************************************************
 * 										EVALUACIONES											*
 ************************************************************************************************/

drop domain if exists d_notas;
create domain d_notas as integer
check (value >= 0 AND value <= 10);

-- CREACION TABLA: EVALUACIONES
create table if not exists evaluaciones(
	id serial primary key,
	dictado int not null references dictados(id),
	fecha date not null,
	evaluador int not null references docentes(legajo)
);


-- CREACION TABLA: EVALUACION_ALUMNOS
create table if not exists evaluacion_alumnos(
	evaluacion int not null references evaluaciones(id),
	alumno int not null references alumnos(legajo),
	ausente bool not null,
	nota d_notas,
    unique (evaluacion, alumno)
); 