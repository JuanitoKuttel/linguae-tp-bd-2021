/************************************************************************************************
 * 										INCIDENCIAS												*
 ************************************************************************************************/

create table if not exists incidencias(
	id serial primary key,
	fecha timestamp default 'now',
	descripcion text not null 
);



/************************************************************************************************
 * 										LOCALIZACIONES											*
 ************************************************************************************************/

-- CREACION TABLA: PAISES
 create table if not exists paises(
	codigo int not null primary key, 
	codigo_alfa varchar(3) unique not null,
	nombre varchar(50) unique not null
);


-- CREACION TABLA: PROVINICIAS 
create table if not exists provincias(
	codigo int not null primary key,
	nombre varchar(50) not null,
	codigo_pais int not null references paises(codigo) 
);


-- CREACION TABLA: CIUDADES 
create table if not exists ciudades(
	codigo int not null primary key,
	nombre varchar(100) not null,
	codigo_provincia int not null references provincias(codigo)
);


-- CREACION TABLA: DIRECCIONES 
create table if not exists direcciones(
	id serial primary key,
    calle varchar(50) not null,
	numero int not null,
	piso int,
	depto varchar(4),
	ciudad int references ciudades(codigo), 
    unique (calle, numero, piso, depto, ciudad)
);



/************************************************************************************************
 * 											SEDES												*
 ************************************************************************************************/

-- CREACION TABLA: SEDES 
create table sedes(
	id serial primary key,
	nombre varchar(100) unique,
	direccion int unique references direcciones(id)
);



/************************************************************************************************
 * 								CARRERAS, CURSOS y DERIVADOS									*
 ************************************************************************************************/

drop domain if exists d_idiomas;
create domain d_idiomas as varchar(25)
check (value in ('INGLES','ALEMAN','ITALIANO'));

drop domain if exists d_niveles;
create domain d_niveles as varchar(25)
check (value in ('BASICO','MEDIO','AVANZADO','ESPECIALISTA'));

drop domain if exists d_modalidades;
create domain d_modalidades as varchar(25)
check (value in ('PRESENCIAL','DISTANCIA','HIBRIDO'));


-- CREACION TABLA: CARRERAS 
create table if not exists carreras(
	id serial primary key,
	nombre varchar(50) not null
);


-- CREACION TABLA: CURSOS 
create table if not exists cursos(
	id serial primary key,
	nombre varchar(50) not null,
	idioma d_idiomas not null,
	nivel d_niveles not null,
	sede int references sedes(id) 
);


-- CREACION DE LA TABLA MODALIDAD_CURSO
create table if not exists modalidades(
	id serial primary key,
	duracion_meses int not null,
	costo money not null,
	modalidad d_modalidades not null
);


--CREACION DE LA TABLA DICTADOS
create table if not exists dictados(
	id serial primary key,
	modalidad int references modalidades(id) not null,
	curso int references cursos(id) not null,
	fec_inicio date not null,
	fec_fin date not null
);


 -- CREACION DE LA TABLA ANO LECTIVO
create table if not exists ano_lectivo(
	id serial primary key,
	carrera int references carreras(id) not null,
	ano int not null,
	monto_inscripcion money not null
);

-- CREACION DE LA TABLA CURSOS CARRERAS
create table if not exists cursos_carreras(
	id_carrera int not null references carreras(id),
	id_curso int not null references cursos(id)
);


-- CREACION DE LA TABLA CORRELATIVIDADES
create table if not exists correlatividades(
	id_curso int not null references cursos(id),
	id_correlativo int not null references cursos(id)
); 



/************************************************************************************************
 * 									PERSONAS Y DERIVADOS										*
 ************************************************************************************************/

drop domain if exists d_rol_docente;
create domain d_rol_docente as varchar(30)
check (value in ('CLASES', 'EVALUADOR'));

drop domain if exists d_cargos;
create domain d_cargos as varchar(30)
check (value in ('SECRETARIO', 'DIRECTOR', 'DIRECTOR GENERAL'));

-- CREACION TABLA: PERSONAS
create table if not exists personas(
	dni int not null primary key,
	nombre varchar(40) not null,
	apellido varchar(40) not null,
	fec_nacimiento date not null
);


-- CREACION TABLA: ALUMNOS
create table if not exists alumnos(
	legajo serial primary key,
	dni int unique not null references personas(dni)
);


-- CREACION TABLA: ALUMNOS <-> SEDE
create table if not exists alumnos_sedes(
    alumno int not null references alumnos(legajo),
    sede int not null references sedes(id)
);


-- CREACION TABLA: DOCENTES
create table if not exists docentes(
	legajo serial primary key,
	dni int unique not null references personas(dni)
);


-- CREACION TABLA: DOCENTES <-> SEDES
create table if not exists docentes_sedes(
    docente int not null references docentes(legajo),
    sede int not null references sedes(id)
);


-- CREACION TABLA: DOCENTES <-> CURSOS
create table if not exists docentes_cursos(
	docente int not null references docentes(legajo),
	curso int not null references dictados(id),
	rol d_rol_docente not null
);


-- CREACION TABLA: CARGOS
create table if not exists cargos(
	dni int primary key references personas(dni),
	cargo d_cargos not null,
    idioma d_idiomas,
	sede int references sedes(id),
    unique (dni, sede)
);



/************************************************************************************************
 * 										EVALUACIONES											*
 ************************************************************************************************/

drop domain if exists d_notas;
create domain d_notas as integer
check (value >= 0 AND value <= 10);

-- CREACION TABLA: EVALUACIONES
create table if not exists evaluaciones(
	id serial primary key,
	dictado int not null references dictados(id),
	fecha date not null,
	evaluador int not null references docentes(legajo)
);


-- CREACION TABLA: EVALUACION_ALUMNOS
create table if not exists evaluacion_alumnos(
	evaluacion int not null references evaluaciones(id),
	alumno int not null references alumnos(legajo),
	ausente bool not null,
	nota d_notas,
    unique (evaluacion, alumno)
); 



/************************************************************************************************
 * 										INSCRIPCIONES											*
 ************************************************************************************************/

-- CREACION DE LA TABLA INSCRIPCIONES
create table if not exists inscripciones_carrera(
	carrera int references carreras(id),
	alumno int references alumnos(legajo)
);

create table if not exists inscripciones_curso(
	curso_dictado int references dictados(id),
	alumno int references alumnos(legajo)
);



/************************************************************************************************
 * 									CUOTAS / PAGOS / SALDO										*
 ************************************************************************************************/

-- CREACION DE LA TABLA CUOTAS CURSOS
create table if not exists cuotas_cursos_alumno(
	id serial primary key,
	costo money not null,
	curso_dictado int not null references dictados(id),
	vencimiento date not null,
	alumno int not null references alumnos(legajo)
);


-- CREACION DE LA TABLA CUOTAS INSCRIPCIONES CARRERAS
create table if not exists cuotas_insc_carrera_alumno(
	id serial primary key,
	costo money not null,
	ano int not null references ano_lectivo(id),
	vencimiento date not null,
	alumno int not null references alumnos(legajo)
);


-- CREACION DE LA TABLA PAGOS CURSOS
create table if not exists pagos_cursos(
	couta_curso int not null references cuotas_cursos_alumno(id),
	monto money not null,
	fecha date default 'now'
);


-- CREACION DE LA TABLA PAGOS CARRERAS
create table if not exists pagos_carrera(
	cuota_insc int not null references cuotas_insc_carrera_alumno(id),
	monto money not null,
	fecha date default 'now' 
);

-- CREACION DE LA TABLA SALDOS
create table if not exists saldos_alumnos(
	alumno int not null references alumnos(legajo),
	saldo money
);
