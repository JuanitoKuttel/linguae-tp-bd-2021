-- INCIDENCIAS
drop table if exists incidencias;

-- EVALUACIONES
drop table if exists evaluacion_alumnos;
drop table if exists evaluaciones;
drop domain if exists d_notas;

-- VIEWS
drop view if exists v_personas;

-- AUDITORIA
drop table if exists registro_docentes;
drop table if exists registro_cargos;

-- CUOTAS PAGOS SALDOS
drop table if exists pagos_cursos;
drop table if exists pagos_carrera;
drop table if exists cuotas_cursos_alumno;
drop table if exists cuotas_insc_carrera_alumno;
drop table if exists saldos_alumnos;

-- INSCRIPCIONES
drop table if exists inscripciones_carrera;
drop table if exists inscripciones_curso;

-- PERSONAS Y DERIVADOS
drop table if exists docentes_cursos;
drop table if exists docentes_sedes;
drop table if exists alumnos_sedes;
drop table if exists alumnos;
drop table if exists docentes;
drop table if exists cargos;
drop table if exists personas;
drop domain if exists d_rol_docente;
drop domain if exists d_cargos;

-- CARRERAS, CURSOS y DERIVADOS
drop table if exists correlatividades;
drop table if exists cursos_carreras;
drop table if exists ano_lectivo;	
drop table if exists dictados;
drop table if exists modalidades;
drop table if exists cursos;
drop table if exists carreras;
drop domain if exists d_idiomas;
drop domain if exists d_niveles;
drop domain if exists d_modalidades;

-- SEDES
drop table if exists sedes;

-- LOCACIONES
drop table if exists direcciones;
drop table if exists ciudades;
drop table if exists provincias;
drop table if exists paises;

